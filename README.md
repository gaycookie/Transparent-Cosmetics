> ⚠️ **ATTENTION, Please read this!**  
> Starting from version 1.20, Transparent Cosmetics will be replaced with [Glass Armor](https://modrinth.com/mod/glass-armor).  
Please note that Transparent Cosmetics will no longer receive updates.

![Mod Banner](https://cdn.modrinth.com/data/Rv02R5P1/images/c2a8b6110ca6cf6a79d0479c94cf116322166f04.png)

### Introduction
Welcome to my first Minecraft mod! As someone who has previously worked on plugins, I wanted to delve into the world of modding and expand my skills. With the help of fellow developers, I present to you my inaugural mod!

### Mod Description
Transparent Cosmetics is a mod designed with PvE gameplay in mind. However, please note that it may be considered overpowered on PvP servers. If you plan to use it on a PvP server, exercise caution and balance it accordingly.

This mod integrates features from [Cosmetic Armor](https://modrinth.com/mod/cosmetic-armor) and [Trinkets](https://modrinth.com/mod/trinkets) to introduce transparent "gear" to your Minecraft modpack. By equipping items from this mod into the cosmetic slots, you can hide the armor you're wearing in your armor slots. This allows you to showcase your skin without having to unequip your armor.

### Technical Information
- Mod Loader: Fabric
- Supported Minecraft Versions: 1.16, 1.17, 1.18, and 1.19

### Changelog
To stay updated on the latest changes, please refer to the [changelog](CHANGELOG.md).

### Dependencies
- [Cosmetic Armor [Fabric]](https://modrinth.com/mod/cosmetic-armor)
- [Fabric API](https://modrinth.com/mod/fabric-api)

### Included in Mod Packs
If you would like to feature this mod in your mod pack, kindly let me know and share it with me.

### Help and Support
For any issues or concerns, please visit the GitLab repository linked under the "source" section and post your problems there. I'll be glad to assist you.

I hope you enjoy using Transparent Cosmetics in your Minecraft adventures!

> ⚠️ **ATTENTION, Please read this!**  
> Starting from version 1.20, Transparent Cosmetics will be replaced with [Glass Armor](https://modrinth.com/mod/glass-armor).  
Please note that Transparent Cosmetics will no longer receive updates.